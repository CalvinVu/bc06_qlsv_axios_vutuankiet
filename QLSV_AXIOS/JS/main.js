const BASE_URL = "https://63f442fcfe3b595e2ef03e59.mockapi.io";

// LAY DU LIEU TU API
function fetchDSSV() {
  axios({
    url: `${BASE_URL}/sv`,
    method: "GET",
  })
    .then(function (res) {
      var DSSV = res.data;
      tatLoading();
      renderDSSV(DSSV);
    })
    .catch(function (err) {
      batLoading();
      console.log(err);
    });
}
fetchDSSV();

// XOA SINH VIEN
function xoaSV(id) {
  console.log(id);
  axios({
    url: `${BASE_URL}/sv/${id}`,
    method: "DELETE",
  })
    .then(function (res) {
      console.log(res);
      tatLoading();
      fetchDSSV();
    })
    .catch(function (err) {
      batLoading();
      console.log(err);
    });
}

// SUA SINH VIEN
function suaSV(id) {
  console.log(id);
  axios({
    url: `${BASE_URL}/sv/${id}`,
    method: "GET",
  })
    .then(function (res) {
      console.log(res);
      showThongTinLenForm(res.data);
    })
    .catch(function (err) {
      console.log(err);
    });
}

// CAP NHAT SINH VIEN
function capNhatSV() {
  var sinhVien = layThongTinTuForm();
  axios({
    url: `${BASE_URL}/sv/${sinhVien.ma}`,
    method: "PUT",
    data: sinhVien,
  })
    .then(function (res) {
      console.log(res);
      fetchDSSV();
    })
    .catch(function (err) {
      console.log(err);
    });
}

// THEM SINH VIEN
function themSV() {
  var sinhVien = layThongTinTuForm();
  axios({
    url: `${BASE_URL}/sv`,
    method: "POST",
    data: sinhVien,
  })
    .then(function (res) {
      console.log(res);
      fetchDSSV();
    })
    .catch(function (err) {
      console.log(err);
    });
}
