// RENDER
function renderDSSV(array) {
  var HTML = "";
  for (var i = 0; i < array.length; i++) {
    var item = array[i];
    HTML += `<tr>
        <td>${item.ma}</td>
        <td>${item.ten}</td>
        <td>${item.email}</td>
        <td></td>
        <td><button class="btn btn-success" id="btnSua" onclick="suaSV(${item.ma})">Sua</button>
        <button class="btn btn-warning" id="btnXoa" onclick="xoaSV(${item.ma})">Xoa</button>
        </td>
        </tr>`;
  }
  document.getElementById("tbodySinhVien").innerHTML = HTML;
}

// LAY THONG TIN TU FORM
function layThongTinTuForm() {
  var ma = document.getElementById("txtMaSV").value;
  var ten = document.getElementById("txtTenSV").value;
  var email = document.getElementById("txtEmail").value;
  var matKhau = document.getElementById("txtPass").value;
  var toan = document.getElementById("txtDiemToan").value;
  var ly = document.getElementById("txtDiemLy").value;
  var hoa = document.getElementById("txtDiemHoa").value;

  var sv = {
    ma: ma,
    ten: ten,
    email: email,
    matKhau: matKhau,
    toan: toan,
    ly: ly,
    hoa: hoa,
  };
  return sv;
}

// SHOW THONG TIN LEN FORM
function showThongTinLenForm(show) {
  document.getElementById("txtMaSV").value = show.ma;
  document.getElementById("txtTenSV").value = show.ten;
  document.getElementById("txtEmail").value = show.email;
  document.getElementById("txtPass").value = show.matKhau;
  document.getElementById("txtDiemToan").value = show.toan;
  document.getElementById("txtDiemLy").value = show.ly;
  document.getElementById("txtDiemHoa").value = show.hoa;
}

// BAT LOADING
function batLoading() {
  document.getElementById("loading").style.display = "block";
}

// TAT LOADING
function tatLoading() {
  document.getElementById("loading").style.display = "none";
}
